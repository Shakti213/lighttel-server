defmodule LighttelServer.Auth.AllowAll do
  @behaviour LighttelServer.Authenticator

  def start_link(_opts) do
    {:ok, nil}
  end

  def new_server_state(nil) do
    %{}
  end

  def authenticate(nil, _, %Lighttel.Packet.PutAuth{auth_method: :userpass} = auth_data) do
    inspect_data = Kernel.inspect(auth_data)
    IO.puts("Username password authentication with data '#{inspect_data}'")
    {:ok, 0, nil}
  end

  def publish_allowed?(nil, _authentications, _endpoint) do
    true
  end

  def remove_authentication(nil, _auth_server_data, _handle) do
    nil
  end
end
