defmodule LighttelServer.Authenticator do
  use GenServer

  @type auth_method :: atom
  @type handle_id :: integer
  @type authentication :: %Lighttel.Packet.PutAuth{}
  @type auth_server_data :: term
  @type authentication_result ::
          {:ok, integer, auth_server_data} | {:error, integer, auth_server_data}
  @callback new_server_state(term) :: {:ok, any}
  @callback start_link(term) :: {:ok, any}
  @callback authenticate(term, auth_server_data, authentication) :: authentication_result
  @callback publish_allowed?(term, auth_server_data, String.t()) :: boolean
  @callback remove_authentication(term, auth_server_data, handle_id) :: auth_server_data

  def start_link(backend \\ LighttelServer.Auth.AllowAll, opts) do
    {:ok, handle} = backend.start_link(opts)
    GenServer.start_link(__MODULE__, {backend, handle}, opts)
  end

  def child_spec(backend, opts) do
    child_spec(opts)
    |> Map.put(:start, {__MODULE__, :start_link, [backend, opts]})
  end

  def new_server_state(server) do
    GenServer.call(server, :initial_state)
  end

  def authenticate(server, server_data, auth_data) do
    GenServer.call(server, {:auth, server_data, auth_data})
  end

  def publish_allowed?(server, endpoint, authentications) do
    GenServer.call(server, {:publish_allowed?, endpoint, authentications})
  end

  def remove_authentication(server, auth_server_data, handle) do
    GenServer.call(server, {:remove_auth, auth_server_data, handle})
  end

  ## GenServer callbacks

  def init(state) do
    {:ok, state}
  end

  def handle_call(:initial_state, _from, {backend, handle} = authenticator) do
    result = backend.new_server_state(handle)
    {:reply, result, authenticator}
  end

  def handle_call({:auth, server_data, auth_data}, _from, {backend, handle} = authenticator) do
    result = backend.authenticate(handle, server_data, auth_data)
    {:reply, result, authenticator}
  end

  def handle_call(
        {:publish_allowed?, endpoint, authentications},
        _from,
        {backend, handle} = authenticator
      ) do
    result = backend.publish_allowed?(handle, endpoint, authentications)
    {:reply, result, authenticator}
  end

  def handle_call(
        {:remove_auth, auth_server_data, handle_id},
        _from,
        {backend, handle} = authenticator
      ) do
    result = backend.remove_authentication(handle, auth_server_data, handle_id)
    {:reply, result, authenticator}
  end
end
