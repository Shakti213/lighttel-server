defmodule LighttelServer.PubSub.PublishSink do
  use GenServer

  @type header_data :: map

  @callback start_link(term) :: {:ok, term}
  @callback publish(term, %Lighttel.Packet.Publish{}) :: :ok | :error

  def start_link(backend, opts) do
    {:ok, handle} = backend.start_link(opts)
    GenServer.start_link(__MODULE__, {backend, handle}, opts)
  end

  def child_spec(backend, opts) do
    child_spec(opts)
    |> Map.put(:start, {__MODULE__, :start_link, [backend, opts]})
  end

  def publish(server, packet) do
    GenServer.call(server, {:publish, packet})
  end

  def init(state) do
    {:ok, state}
  end

  def handle_call({:publish, packet}, _from, {backend, handle} = state) do
    result = backend.publish(handle, packet)
    {:reply, result, state}
  end
end
