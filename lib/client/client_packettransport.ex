defmodule LighttelServer.Client.PacketTransport do
  use Agent, restart: :temporary

  alias LighttelServer.Client.Timer
  alias LighttelServer.Client.ClientHandler

  def start_link(sender, opts) do
    max_size = Keyword.get(opts, :max_size, 2048 * 4)

    Agent.start_link(
      fn ->
        me = self()
        {:ok, timer_handle} = Timer.start_link()
        {:ok, handler} = ClientHandler.start_link(fn x -> write(me, x) end, [])

        state = %{
          :binary_send => sender,
          :binary_data => <<>>,
          :max_size => max_size,
          :timers => timer_handle,
          :packet_handler => &on_connect_packet/2,
          :idle_timeout => 60,
          :client_handler => handler
        }

        reload_idle_timer(state)
        state
      end,
      opts
    )
  end

  def write(agent, to_write) do
    Agent.update(agent, fn x ->
      handle_write(x, to_write)
      x
    end)
  end

  def on_binary_data(agent, data) do
    Agent.get_and_update(agent, fn x -> handle_on_binary_data(x, data) end)
  end

  defp handle_write(state, data) when is_binary(data) do
    :ok = state.binary_send.(data)
    :ok = reload_ping_timer(state)
    :ok
  end

  defp handle_write(state, data) when is_map(data) do
    handle_write(state, Lighttel.Packet.to_binary!(data))
  end

  defp handle_on_binary_data(%{:binary_data => stored_data} = state, data) do
    {:ok, new_state} = binary_data_loop(stored_data <> data, state)
    reload_idle_timer(new_state)
    {:ok, new_state}
  end

  defp binary_data_loop(data, state) do
    case Lighttel.Packet.from_binary(data) do
      {:ok, packet, rest} ->
        case packet |> state.packet_handler.(state) do
          {:ok, newstate} ->
            binary_data_loop(rest, newstate)

          {:reply, to_send, newstate} ->
            to_send
            |> Enum.each(fn p ->
              :ok = handle_write(newstate, p)
            end)

            binary_data_loop(rest, newstate)

          error ->
            error
        end

      {:error, :need_more_data, _bin} ->
        {:ok, data} = inspect_header(data, state.max_size)
        {:ok, Map.put(state, :binary_data, data)}

      error ->
        error
    end
  end

  defp inspect_header(data, max_size) do
    case Lighttel.Packet.FixedHeader.from_binary(data) do
      {:ok, header, _rest} ->
        if header.remaining_length < max_size do
          {:ok, data}
        else
          {:error, :packet_too_big}
        end

      {:error, :need_more_data, _bin} ->
        {:ok, data}

      error ->
        error
    end
  end

  defp on_connect_packet(connect = %Lighttel.Packet.Connect{}, state) do
    IO.puts("Connect!")

    my_idle_timeout =
      connect.req_remote_connection_idle_timeout
      |> max(60)
      |> min(1200)

    newstate =
      state
      |> Map.put(:packet_handler, &main_packet_handler/2)
      |> Map.put(:ping_timeout, connect.connection_idle_timeout / 2.5)
      |> Map.put(:idle_timeout, my_idle_timeout)

    {:reply, [ack]} = ClientHandler.handle_packet(state.client_handler, connect)

    ack =
      ack
      |> Map.put(:maximum_packet_size, state.max_size)
      |> Map.put(:connection_idle_timeout, my_idle_timeout)

    {:reply, [ack], newstate}
  end

  defp main_packet_handler(packet, state) do
    case ClientHandler.handle_packet(state.client_handler, packet) do
      {:reply, packets} -> {:reply, packets, state}
      :ok -> {:ok, state}
    end
  end

  defp reload_idle_timer(state) do
    me = self()

    :started =
      Timer.start_timer(state.timers, :singleshot, :idle_timer, state.idle_timeout, fn _ ->
        Agent.stop(me, :idle_timeout)
      end)

    :ok
  end

  defp reload_ping_timer(state) do
    me = self()

    :started =
      Timer.start_timer(state.timers, :singleshot, :ping_timer, state.ping_timeout, fn _ ->
        IO.puts("PING!")
        write(me, %Lighttel.Packet.Ping{})
      end)

    :ok
  end
end
