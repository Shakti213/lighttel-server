defmodule LighttelServer.TcpClient do
  use GenServer, restart: :temporary

  def start_link(opts) do
    GenServer.start_link(__MODULE__, Keyword.fetch!(opts, :socket), opts)
  end

  def init(socket) do
    sender = fn x when is_binary(x) ->
      :gen_tcp.send(socket, x)
    end

    {:ok, client} = LighttelServer.Client.PacketTransport.start_link(sender, [])
    {:ok, {socket, client}}
  end

  def handle_info({:tcp, _, data}, {_socket, client} = state) do
    :ok = LighttelServer.Client.PacketTransport.on_binary_data(client, data)
    {:noreply, state}
  end

  def handle_info({:tcp_closed, _}, state) do
    {:stop, :normal, state}
  end

  def handle_info({:tcp_error, _, error}, state) do
    {:stop, {:tcp_error, error}, state}
  end

  def handle_info({:tcp_passive, _}, state) do
    {:stop, {:tcp_error, :tcp_passive_not_supported}, state}
  end
end
