# Lighttel Server

Work in progress server that implements the [lighttel](https://gitlab.com/Shakti213/lighttel-specification) protocol.

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `lighttel_server` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:lighttel_server, "~> 0.1.0"}
  ]
end
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/lighttel_server](https://hexdocs.pm/lighttel_server).

