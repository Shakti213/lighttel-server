defmodule LighttelServer.TcpTest do
  use ExUnit.Case
  doctest LighttelServer.Tcp

  @moduletag :capture_log

  @test_user "test"
  @test_pass "test"

  alias __MODULE__

  defmodule TestAuthenticator do
    @behaviour LighttelServer.Authenticator

    def start_link(_) do
      {:ok, nil}
    end

    def new_server_state(_) do
      %{}
    end

    def authenticate(_, authentications, %Lighttel.Packet.PutAuth{
          handle: handle,
          auth_method: :userpass,
          auth_data: auth_data
        }) do
      authentications = if authentications == nil, do: %{}, else: authentications

      user_pass_map = %{
        "test" => "test",
        "test2" => "test2"
      }

      stored_password = user_pass_map[auth_data.username]

      if stored_password !== nil and auth_data.password === stored_password do
        {:ok, 0, Map.put(authentications, handle, auth_data.username)}
      else
        {:nack, 0, authentications}
      end
    end

    defp is_superuser(authentications) do
      Enum.find(authentications, nil, fn {_k, v} ->
        v === "test"
      end) !== nil
    end

    defp allowed_for_endpoint(authentications, endpoint) do
      ep_user_map = %{
        "@test" => ["test"],
        "@test2" => ["test2"]
      }

      auth_users =
        authentications
        |> Enum.map(fn {_k, v} -> v end)
        |> MapSet.new()

      users = Map.get(ep_user_map, endpoint, [])

      if users === :all do
        true
      else
        users = MapSet.new(users)
        ep_auth_users = MapSet.intersection(auth_users, users)

        MapSet.size(ep_auth_users) > 0
      end
    end

    def publish_allowed?(_, authentications, endpoint) do
      is_superuser(authentications) or allowed_for_endpoint(authentications, endpoint)
    end

    def remove_authentication(_, authentications, handle) do
      Map.delete(authentications, handle)
    end
  end

  defmodule TestPublishSink do
    @behaviour LighttelServer.PubSub.PublishSink
    use Agent

    def start_link(_opts) do
      Agent.start_link(fn -> [] end, name: __MODULE__)
    end

    def publish(agent, data) do
      Agent.update(agent, &List.insert_at(&1, -1, data))
    end

    def take_all() do
      Agent.get_and_update(__MODULE__, fn set -> {set, []} end)
    end

    def length() do
      Agent.get(__MODULE__, &length(&1))
    end
  end

  setup do
    Application.stop(:lighttel_server)

    Application.put_env(
      :lighttel_server,
      :authenticator,
      LighttelServer.TcpTest.TestAuthenticator,
      persistent: true
    )

    Application.put_env(
      :lighttel_server,
      :publish_backend,
      LighttelServer.TcpTest.TestPublishSink,
      persistent: true
    )

    :ok = Application.start(:lighttel_server)
  end

  setup do
    opts = [:binary, active: false]
    {:ok, socket} = :gen_tcp.connect('localhost', 9321, opts)
    %{socket: socket}
  end

  test "Connect responds with ACK", %{socket: socket} do
    # Note that we want to test with pattern matching, to make sure
    # we receive a Connect ACK, not what the actual values are
    assert %Lighttel.Packet.Connect.Ack{} = send_recv(socket, %Lighttel.Packet.Connect{})
  end

  test "Handle slow and split messages", %{socket: socket} do
    {:ok, to_send} = Lighttel.Packet.to_binary(%Lighttel.Packet.Connect{})
    bin_list = :binary.bin_to_list(to_send)

    bin_list
    |> Enum.each(fn b ->
      Process.sleep(50)
      :gen_tcp.send(socket, <<b>>)
    end)

    {:ok, response} = :gen_tcp.recv(socket, 0, 5000)

    assert {:ok, %Lighttel.Packet.Connect.Ack{}, _rest} =
             Lighttel.Packet.from_binary(response, [])
  end

  test "Non-connect first message disconnects", %{socket: socket} do
    to_send = Lighttel.Packet.to_binary!(%Lighttel.Packet.Connect.Ack{})
    :gen_tcp.send(socket, to_send)
    assert {:error, :closed} == :gen_tcp.recv(socket, 0, 5000)
  end

  test "Connection setup and auth ok", %{socket: socket} do
    assert :ok == setup_connection(socket)
    assert :ok == authenticate_ok(socket, @test_user, @test_pass)
  end

  test "Bad auth results in Nack", %{socket: socket} do
    assert :ok == setup_connection(socket)
    assert :ok == authenticate_fail(socket, "abc", "def")
  end

  test "Basic publish", %{socket: socket} do
    assert :ok == setup_connection(socket)
    assert :ok == authenticate_ok(socket, @test_user, @test_pass)

    resp =
      send_recv(socket, %Lighttel.Packet.Publish{
        endpoint: "@test",
        payload: "hello",
        message_id: 129
      })

    assert resp === %Lighttel.Packet.Publish.Ack{message_ids: [129]}
    assert TcpTest.TestPublishSink.length() === 1
    [pub] = TcpTest.TestPublishSink.take_all()
    assert pub === %Lighttel.Packet.Publish{endpoint: "@test", payload: "hello", message_id: 129}
  end

  test "Multiple publish", %{socket: socket} do
    assert :ok == setup_connection(socket)
    assert :ok == authenticate_ok(socket, @test_user, @test_pass)

    resp =
      send_recv(socket, %Lighttel.Packet.Publish{
        endpoint: "@test",
        payload: "hello",
        message_id: 129
      })

    resp2 =
      send_recv(socket, %Lighttel.Packet.Publish{
        endpoint: "@test2",
        payload: "world",
        message_id: 130
      })

    assert resp === %Lighttel.Packet.Publish.Ack{message_ids: [129]}
    assert resp2 === %Lighttel.Packet.Publish.Ack{message_ids: [130]}
    assert TcpTest.TestPublishSink.length() === 2
    [pub1, pub2] = TcpTest.TestPublishSink.take_all()
    assert pub1 === %Lighttel.Packet.Publish{endpoint: "@test", payload: "hello", message_id: 129}

    assert pub2 === %Lighttel.Packet.Publish{
             endpoint: "@test2",
             payload: "world",
             message_id: 130
           }
  end

  test "Batch publish", %{socket: socket} do
    assert :ok == setup_connection(socket)
    assert :ok == authenticate_ok(socket, @test_user, @test_pass)

    messages = [
      %Lighttel.Packet.Publish{endpoint: "@test", payload: "hello", message_id: 129},
      %Lighttel.Packet.Publish{endpoint: "@test2", payload: "world", message_id: 130}
    ]

    resp = send_recv(socket, %Lighttel.Packet.BatchPublish{messages: messages})
    assert resp === %Lighttel.Packet.Publish.Ack{message_ids: [129, 130]}
    assert TcpTest.TestPublishSink.length() === 2
    [pub1, pub2] = TcpTest.TestPublishSink.take_all()
    assert pub1 === %Lighttel.Packet.Publish{endpoint: "@test", payload: "hello", message_id: 129}

    assert pub2 === %Lighttel.Packet.Publish{
             endpoint: "@test2",
             payload: "world",
             message_id: 130
           }
  end

  test "Publish to endpoint handle", %{socket: socket} do
    assert :ok == setup_connection(socket)
    assert :ok == authenticate_ok(socket, @test_user, @test_pass)

    put_handle =
      send_recv(socket, %Lighttel.Packet.PutPublishHandle{
        handles: %{1 => "@test", 2 => "@test2"}
      })

    assert put_handle === %Lighttel.Packet.PutPublishHandle.Ack{results: %{1 => 0, 2 => 0}}

    resp =
      send_recv(socket, %Lighttel.Packet.Publish{endpoint: 1, payload: "hello", message_id: 129})

    resp2 =
      send_recv(socket, %Lighttel.Packet.Publish{endpoint: 2, payload: "world", message_id: 130})

    assert resp === %Lighttel.Packet.Publish.Ack{message_ids: [129]}
    assert resp2 === %Lighttel.Packet.Publish.Ack{message_ids: [130]}

    assert TcpTest.TestPublishSink.length() === 2
    [pub1, pub2] = TcpTest.TestPublishSink.take_all()
    assert pub1 === %Lighttel.Packet.Publish{endpoint: "@test", payload: "hello", message_id: 129}

    assert pub2 === %Lighttel.Packet.Publish{
             endpoint: "@test2",
             payload: "world",
             message_id: 130
           }
  end

  test "Publish to unallowed not handled", %{socket: socket} do
    assert :ok == setup_connection(socket)
    assert :ok == authenticate_ok(socket, "test2", "test2")

    messages = [
      %Lighttel.Packet.Publish{endpoint: "@test", payload: "hello", message_id: 129},
      %Lighttel.Packet.Publish{endpoint: "@test2", payload: "world", message_id: 130}
    ]

    resp = send_recv(socket, %Lighttel.Packet.BatchPublish{messages: messages})
    assert resp === %Lighttel.Packet.Publish.Ack{message_ids: [130]}
    assert TcpTest.TestPublishSink.length() === 1
    [pub] = TcpTest.TestPublishSink.take_all()
    assert pub === %Lighttel.Packet.Publish{endpoint: "@test2", payload: "world", message_id: 130}
  end

  test "Compressed batch publish", %{socket: socket} do
    assert :ok == setup_connection(socket)
    assert :ok == authenticate_ok(socket, @test_user, @test_pass)

    messages = [
      %Lighttel.Packet.Publish{endpoint: "@test", payload: "hello", message_id: 129},
      %Lighttel.Packet.Publish{endpoint: "@test2", payload: "world", message_id: 130}
    ]

    resp =
      send_recv(socket, %Lighttel.Packet.BatchPublish{messages: messages}, compression: :deflate)

    assert resp === %Lighttel.Packet.Publish.Ack{message_ids: [129, 130]}
    assert TcpTest.TestPublishSink.length() === 2
    [pub1, pub2] = TcpTest.TestPublishSink.take_all()
    assert pub1 === %Lighttel.Packet.Publish{endpoint: "@test", payload: "hello", message_id: 129}

    assert pub2 === %Lighttel.Packet.Publish{
             endpoint: "@test2",
             payload: "world",
             message_id: 130
           }
  end

  defp send_recv(socket, to_send, opts \\ []) when is_map(to_send) do
    send_type = to_send.__struct__
    {:ok, to_send} = Lighttel.Packet.to_binary(to_send, opts)

    if send_type === Lighttel.Packet.BatchPublish do
      IO.inspect(opts)
      IO.inspect(byte_size(to_send))
    end

    :gen_tcp.send(socket, to_send)
    {:ok, response} = :gen_tcp.recv(socket, 0, 5000)
    {:ok, packet, _rest} = Lighttel.Packet.from_binary(response, [])
    packet
  end

  defp setup_connection(socket) do
    alias Lighttel.Packet
    %Packet.Connect.Ack{} = send_recv(socket, %Packet.Connect{})
    :ok
  end

  defp authenticate_ok(socket, username, password) do
    alias Lighttel.Packet

    auth_data = %{
      username: username,
      password: password
    }

    %Packet.PutAuth.Ack{} =
      send_recv(socket, %Packet.PutAuth{
        handle: 1,
        auth_data: auth_data
      })

    :ok
  end

  defp authenticate_fail(socket, username, password) do
    alias Lighttel.Packet

    auth_data = %{
      username: username,
      password: password
    }

    %Packet.PutAuth.Nack{} =
      send_recv(socket, %Packet.PutAuth{
        handle: 1,
        auth_data: auth_data
      })

    :ok
  end
end
