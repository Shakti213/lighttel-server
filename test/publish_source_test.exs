defmodule LighttelServer.PubSub.PublishSourceTest do
  use ExUnit.Case

  defmodule TestSource do
    use Agent

    @behaviour LighttelServer.PubSub.PublishSource

    @impl true
    def start_link(_opts) do
      Agent.start_link(fn -> %{:subscribe => MapSet.new(), :ack => []} end, name: TestSource)
    end

    @impl true
    def subscribe(agent, endpoint, _opts) do
      ep_list = if is_list(endpoint), do: endpoint, else: [endpoint]

      Agent.update(agent, fn state ->
        new_sub =
          ep_list
          |> Enum.reduce(state[:subscribe], &MapSet.put(&2, &1))

        Map.put(state, :subscribe, new_sub)
      end)

      :ok
    end

    @impl true
    def ack(agent, uid) do
      Agent.update(agent, fn state ->
        new_ack = state[:ack] ++ [uid]
        Map.put(state, :ack, new_ack)
      end)

      :ok
    end

    @impl true
    def unsubscribe(agent, ep) do
      Agent.update(agent, fn state ->
        new_sub =
          state[:subscribe]
          |> MapSet.delete(ep)

        Map.put(state, :subscribe, new_sub)
      end)

      :ok
    end

    def subscriptions(agent) do
      Agent.get(agent, fn state ->
        state[:subscribe]
      end)
    end
  end

  alias LighttelServer.PubSub.PublishSource

  setup do
    {:ok, ps} =
      LighttelServer.PubSub.PublishSource.start_link(
        LighttelServer.PubSub.PublishSourceTest.TestSource,
        []
      )

    %{ps: ps}
  end

  test "Basic subscribe", %{ps: ps} do
    assert LighttelServer.PubSub.PublishSource.subscribe(ps, "@hello", []) === :ok
    assert TestSource.subscriptions(TestSource) === MapSet.new(["@hello"])
  end

  test "Multiple subscribe", %{ps: ps} do
    assert PublishSource.subscribe(ps, ["@hello", "@world"], []) === :ok
    assert TestSource.subscriptions(TestSource) === MapSet.new(["@hello", "@world"])
    assert PublishSource.subscribe(ps, ["@hello", "@world"], []) === :ok
    assert TestSource.subscriptions(TestSource) === MapSet.new(["@hello", "@world"])
  end

  test "Message arrives", %{ps: ps} do
    :ok = PublishSource.subscribe(ps, ["@hello"], [])
    to_publish = [{1, "@hello", "WORLD!"}, {2, "@hello", "Wheeee"}]
    send(ps, {:publish, to_publish})

    receive do
      {:publish, messages} ->
        assert length(messages) === 2
        assert to_publish === messages
    end
  end

  test "Messages are filtered", %{ps: ps} do
    :ok = PublishSource.subscribe(ps, ["@hello"], [])
    to_publish = [{1, "@hello", "WORLD!"}, {2, "@hello", "Wheeee"}]
    filtered = [{1, "@world", "filtered data"}]
    to_publish2 = [{3, "@hello", "not filtered data"}]
    send(ps, {:publish, to_publish ++ filtered ++ to_publish2})

    receive do
      {:publish, messages} ->
        assert length(messages) === length(to_publish) + length(to_publish2)
        assert messages === to_publish ++ to_publish2
    end
  end
end
